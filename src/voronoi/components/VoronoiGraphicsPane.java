package voronoi.components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JComponent;

import voronoi.constants.Colors;
import voronoi.graphics.DrawableElement;
import voronoi.grid.AdvancedCoordinateTranslator;
import voronoi.grid.CoordinateTranslator;

public class VoronoiGraphicsPane extends JComponent {

	private static final long serialVersionUID = -2801042729218318371L;

	private final List<DrawableElement> elements;
	private BufferedImage image;
	private CoordinateTranslator translator;
	
	public VoronoiGraphicsPane(int width, int height) {
		elements = new ArrayList<DrawableElement>();
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		translator = new AdvancedCoordinateTranslator(width, height, 5000.0);
	}
	
	@Override
	public void paint(Graphics graphics) {
		Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.drawImage(image, 0, 0, null);
	}
	
	public void render() {
		Graphics2D graphics2D = image.createGraphics();
		graphics2D.setColor(Colors.mainBackgroundColor);
		graphics2D.fillRect(0, 0, image.getWidth(), image.getHeight());
		for (DrawableElement element : elements)
			element.draw(graphics2D, translator);
	}
	
	public void addDrawableComponent(DrawableElement element) {
		elements.add(element);
	}
	
	public void setMaxGridSize(double gridSize) {
		translator.setMaxGridSize(gridSize);
	}
	
	public void setScreenSize(int width, int height) {
		translator.setScreenSize(width, height);
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		render();
		repaint();
	}
	
	public boolean zoom(double zoomLevel, int mouseX, int mouseY) {
		Point2D.Double mousePositionInGrid = translator.translatePoint(mouseX, mouseY);
		if (mousePositionInGrid == null)
			return false;
		
		final Rectangle2D.Double gridView = translator.getCurrentGridView();
		final double currentGridsize = gridView.width;
		final double targetGridsize = zoomLevel*currentGridsize;
		final double maxGridsize = translator.getMaxGridSize();
		if (targetGridsize >= maxGridsize) {
			gridView.setRect(0, 0, maxGridsize, maxGridsize);
			translator.setGridView(gridView);
			return true;
		}
		
		final double diffGridsize = currentGridsize - targetGridsize;
		final double xPct = (mousePositionInGrid.x - gridView.x) / currentGridsize;
		final double yPct = (mousePositionInGrid.y - gridView.y) / currentGridsize;
		double gridX = gridView.x + xPct * diffGridsize;
		double gridY = gridView.y + yPct * diffGridsize;
		if (gridX < 0)
			gridX = 0;
		if (gridY < 0) {
			gridY = 0;
		}
		if (gridX + targetGridsize > maxGridsize)
			gridX = maxGridsize - targetGridsize;
		if (gridY + targetGridsize > maxGridsize) {
			gridY = maxGridsize - targetGridsize;
		}
		
		gridView.setRect(gridX, gridY, targetGridsize, targetGridsize);
		translator.setGridView(gridView);
		return true;
	}
	
	public boolean drag(int dragX, int dragY) {
		final double distanceX = translator.translateDistance(dragX);
		final double distanceY = translator.translateDistance(dragY);
		final Rectangle2D.Double gridView = translator.getCurrentGridView();
		final double maxGridsize = translator.getMaxGridSize();
		
		double newX = gridView.x + distanceX;
		if (newX < 0)
			newX = 0;
		if (newX + gridView.width > maxGridsize)
			newX = maxGridsize - gridView.width;
		double newY = gridView.y + distanceY;
		if (newY < 0)
			newY = 0;
		if (newY + gridView.height > maxGridsize)
			newY = maxGridsize - gridView.height;
		
		if (newX == gridView.x && newY == gridView.y)
			return false;
		
		gridView.setRect(newX, newY, gridView.width, gridView.height);
		translator.setGridView(gridView);
		
		return true;
	}
	
}
