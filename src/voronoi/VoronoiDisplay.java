package voronoi;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import voronoi.components.VoronoiGraphicsPane;
import voronoi.graphics.ComparableLine2D;
import voronoi.graphics.DrawableElement;
import voronoi.graphics.DrawableCoordinateGrid;
import voronoi.graphics.DrawablePoint2D;
import voronoi.persistence.VoronoiFileParser;

public class VoronoiDisplay extends JFrame implements ComponentListener{
	
	private static VoronoiDisplay instance;
	
	private static Map<Integer, DrawablePoint2D> pointMap = new HashMap<Integer, DrawablePoint2D>();
	private static List<ComparableLine2D> lineList  = new ArrayList<ComparableLine2D>();
	
	public static final VoronoiDisplay getInstance() {
		return instance;
	}
	
	static {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				instance = new VoronoiDisplay();
				instance.addDrawableComponent(new DrawableCoordinateGrid());
				VoronoiFileParser.parse("ausgabe2");
				instance.renderGraphicsPane();
				instance.repaint();
			}
		});
	}
	
	private static final long serialVersionUID = 582551272932222359L;

	public static void main(String[] args) {
		
	}
	
	private final VoronoiGraphicsPane graphicsPane;
	private final InputHandler inputHandler;
	private final Timer resizeTimer;
	private int pointNum = 1;
	
	private VoronoiDisplay() {
        super("VoronoiDisplay");
        Dimension screenDimensions = Toolkit.getDefaultToolkit().getScreenSize();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        inputHandler = new InputHandler();
        addKeyListener(inputHandler);
        
        setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    c.fill = GridBagConstraints.BOTH;
	    c.weightx = 1.0;
	    c.weighty = 1.0;
	    graphicsPane = new VoronoiGraphicsPane(screenDimensions.width / 2, screenDimensions.height / 2);
	    graphicsPane.addMouseListener(inputHandler);
	    graphicsPane.addMouseMotionListener(inputHandler);
	    graphicsPane.addMouseWheelListener(inputHandler);
	    add(graphicsPane, c);
	    setMinimumSize(new Dimension(300,300));
	    pack();
	    
	    resizeTimer = new Timer(100, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				graphicsPane.setScreenSize(graphicsPane.getWidth(), graphicsPane.getHeight());
			}
	    	
	    });
	    resizeTimer.setRepeats(false);
	    
	    setSize(screenDimensions.width / 2, screenDimensions.height / 2);
	    addComponentListener(this);
        setLocation(new Point(screenDimensions.width / 4,screenDimensions.height / 4));
        setVisible(true);
	}
	
	public void setMaxGridSize(double gridSize) {
		graphicsPane.setMaxGridSize(gridSize);
	}
	
	public void addDrawableComponent(DrawableElement component) {
		graphicsPane.addDrawableComponent(component);
	}
	
	public boolean addLine2D(ComparableLine2D line2d) {
		if (lineList.contains(line2d))
			return false;
		lineList.add(line2d);
		return true;
	}
	
	public void addVoronoiPoint(DrawablePoint2D point) {
		pointMap.put(pointNum, point);
		pointNum++;
	}
	
	public int getVoronoiPoints() {
		return pointNum - 1;
	}
	
	public DrawablePoint2D getVoronoiPoint(int num) {
		return pointMap.get(num);
	}
	
	public void renderGraphicsPane() {
		graphicsPane.render();
	}
	
	public void handleZoom(double zoomLevel, int mouseX, int mouseY) {
		if (graphicsPane.zoom(zoomLevel, mouseX, mouseY)) {
			graphicsPane.render();
			repaint();
		}
			
	}
	
	public void handleDrag(int dragX, int dragY) {
		if (graphicsPane.drag(dragX, dragY)) {
			graphicsPane.render();
			repaint();
		}
	}

	@Override
	public void componentHidden(ComponentEvent event) {
	}

	@Override
	public void componentMoved(ComponentEvent event) {
	}

	@Override
	public void componentResized(ComponentEvent event) {
		resizeTimer.restart();
	}

	@Override
	public void componentShown(ComponentEvent event) {
	}
	
}
