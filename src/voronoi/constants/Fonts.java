package voronoi.constants;

import java.awt.Font;

public class Fonts {
	
	public static final Font genericTextFont = new Font("serif",Font.BOLD | Font.ITALIC, 12);
	public static final Font smallTextFont = new Font("serif",Font.PLAIN, 10);

}
