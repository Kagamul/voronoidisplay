package voronoi.constants;

import java.awt.Color;

public class Colors {
	public static final Color mainBackgroundColor = new Color(128,128,128);
	public static final Color gridBackgroundColor = new Color(230,235,255);
	public static final Color primaryGridColor = new Color(0, 50, 255);
	public static final Color secondaryGridColor = new Color(200, 210, 255);
	public static final Color gridBoundaryColor = new Color(0,32,128);
	public static final Color textBackgroundColor = new Color(230,235,255,198);
}
