package voronoi.grid;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import voronoi.grid.clipping.PolygonClipping;

public class AdvancedCoordinateTranslator implements CoordinateTranslator{

	private double maxGridSize;
	private double currentGridSize;
	private double currentGridOffsetX, currentGridOffsetY;
	private int screenSize;
	private int screenOffsetX, screenOffsetY;
	
	public AdvancedCoordinateTranslator(int width, int height, double initialGridsize) {
		setScreenSize(width, height);
		setMaxGridSize(initialGridsize);
	}
	
	@Override
	public double translateDistance(int distance) {
		return (double)distance * currentGridSize / (double)screenSize ;
	}
	
	@Override
	public Point translatePoint(double x, double y) {
		if (x < currentGridOffsetX || y < currentGridOffsetY || x > (currentGridOffsetX + currentGridSize) || y > (currentGridOffsetY + currentGridSize))
			return null;
		return translateOffscreenPoint(x, y);
	}
	
	@Override
	public Point translateOffscreenPoint(double x, double y) {
		final int px = (int)Math.round(screenSize * (x - currentGridOffsetX) / currentGridSize) + screenOffsetX;
		final int py = (int)Math.round(screenSize * (y - currentGridOffsetY) / currentGridSize) + screenOffsetY;
		return new Point(px,screenSize -py);
	}

	@Override
	public Point[] translateLine(double x1, double y1, double x2, double y2) {
		final Point[] point = new Point[2];
		point[0] = null;
		point[1] = null;
		int px, py;
		final double currentGridX2 = currentGridOffsetX + currentGridSize;
		final double currentGridY2 = currentGridOffsetY + currentGridSize;
		if (x1 >= currentGridOffsetX && y1 >= currentGridOffsetY && y2 >= currentGridOffsetY && x2 >= currentGridOffsetX)
			if (x1 <= currentGridX2 && y1 <= currentGridY2 && x2 <= currentGridX2 && y2 <= currentGridY2) { // line completely inside gridView
				px = (int)Math.round(screenSize * (x1 - currentGridOffsetX) / currentGridSize) + screenOffsetX;
				py = (int)Math.round(screenSize * (y1 - currentGridOffsetY) / currentGridSize) + screenOffsetY;
				point[0] = new Point(px, screenSize - py);
				px = (int)Math.round(screenSize * (x2 - currentGridOffsetX) / currentGridSize) + screenOffsetX;
				py = (int)Math.round(screenSize * (y2 - currentGridOffsetY) / currentGridSize) + screenOffsetY;
				point[1] = new Point(px, screenSize - py);
				return point;
			}
		final double minx = Math.min(x1, x2);
		final double maxx = Math.max(x1, x2);
		final double miny = Math.min(y1, y2);
		final double maxy = Math.max(y1, y2);
		if (maxx < currentGridOffsetX || minx > currentGridX2 || maxy < currentGridOffsetY || miny > currentGridY2) // line bounds completely outside gridView
			return point;
		final Line2D.Double line = crop(new Line2D.Double(x1,y1,x2,y2));
		if (line == null)
			return point;
		px = (int)Math.round(screenSize * (line.x1 - currentGridOffsetX) / currentGridSize) + screenOffsetX;
		py = (int)Math.round(screenSize * (line.y1 - currentGridOffsetY) / currentGridSize) + screenOffsetY;
		point[0] = new Point(px, screenSize - py);
		px = (int)Math.round(screenSize * (line.x2 - currentGridOffsetX) / currentGridSize) + screenOffsetX;
		py = (int)Math.round(screenSize * (line.y2 - currentGridOffsetY) / currentGridSize) + screenOffsetY;
		point[1] = new Point(px, screenSize - py);
		return point;
	}
	
	@Override
	public Rectangle translateRectangle(double x, double y, double width, double height) {
		int rectangleX, rectangleY, rectangleWidth, rectangleHeight;
		double currentGridMaxX = currentGridOffsetX + currentGridSize;
		double currentGridMaxY = currentGridOffsetY + currentGridSize;
		double x2 = x + width;
		double y2 = y + height;
		if (x2 < currentGridOffsetX || x > currentGridMaxX || y2 < currentGridOffsetY || y > currentGridMaxY)
			return null;
		if (x < currentGridOffsetX)
			x = currentGridOffsetX;
		if (y < currentGridOffsetY)
			y = currentGridOffsetY;
		if (x2 > currentGridMaxX)
			x2 = currentGridMaxX;
		if (y2 > currentGridMaxY)
			y2 = currentGridMaxY;
		rectangleX = (int)Math.round(screenSize * (x - currentGridOffsetX) / currentGridSize) + screenOffsetX;
		rectangleY = (int)Math.round(screenSize * (y - currentGridOffsetY) / currentGridSize) - screenOffsetY;
		rectangleWidth = (int)Math.round(screenSize * (x2 - currentGridOffsetX) / currentGridSize) + screenOffsetX - rectangleX;
		rectangleHeight = (int)Math.round(screenSize * (y2 - currentGridOffsetY) / currentGridSize) - screenOffsetY - rectangleY;
		return new Rectangle(rectangleX, rectangleY, rectangleWidth, rectangleHeight);
	}

	@Override
	public Rectangle2D.Double getCurrentGridView() {
		return new Rectangle2D.Double(currentGridOffsetX, currentGridOffsetY, currentGridSize, currentGridSize);
	}

	@Override
	public double getMaxGridSize() {
		return maxGridSize;
	}

	@Override
	public int getCurrentScreenSize() {
		return screenSize;
	}

	@Override
	public void setMaxGridSize(double gridSize) {
		maxGridSize = gridSize;
		currentGridSize = gridSize;
		currentGridOffsetX = 0;
		currentGridOffsetY = 0;
	}

	@Override
	public void setScreenSize(int width, int height) {
		if (width > height) {
			screenSize = height -21;
			screenOffsetX = (width - height) / 2 + 10;
			screenOffsetY = - 10;
		} else {
			screenSize = width -21;
			screenOffsetX = 10;
			screenOffsetY = - (height - width) / 2 - 10;
		}
	}

	@Override
	public Point2D.Double translatePoint(int x, int y) {
		x -= screenOffsetX;
		y += screenOffsetY;
		if (x < 0 || y < 0 || x >= screenSize || y >= screenSize) // Point outside of grid
			return null;
		final double gx = (double) x * currentGridSize / (double) screenSize + currentGridOffsetX;
		final double gy = (double) y * currentGridSize / (double) screenSize - currentGridOffsetY;
		return new Point2D.Double(gx, currentGridSize - gy);
	}

	@Override
	public void setGridView(Rectangle2D.Double gridView) {
		currentGridSize = gridView.width;
		currentGridOffsetX = gridView.x;
		currentGridOffsetY = gridView.y;
	}
	
	public Point2D.Double getIntersectionPoint(Line2D.Double line1,
			Line2D.Double line2) {
		if (!line1.intersectsLine(line2))
			return null;
		double px = line1.getX1(), 
			py = line1.getY1(), 
			rx = line1.getX2() - px, 
			ry = line1.getY2() - py;
		double qx = line2.getX1(), 
			qy = line2.getY1(), 
			sx = line2.getX2() - qx, 
			sy = line2.getY2() - qy;

		double det = sx * ry - sy * rx;
		if (det == 0) {
			return null;
		} else {
			double z = (sx * (qy - py) + sy * (px - qx)) / det;
			if (z >= 0 || z <= 1)
				return new Point2D.Double((float) (px + z * rx), (float) (py + z* ry));
			return null;
		}
	}
	
	public Line2D.Double crop(Line2D.Double line) {
		Point2D.Double p1 = null, p2 = null;
		
		double x1 = currentGridOffsetX;
		double x2 = currentGridSize + x1;
		double y1 = currentGridOffsetY;
		double y2 = currentGridSize + y1;
		
		if (line.x1 >= x1 && line.x1 < x2 && line.y1 >= y1 && line.y1 < y2)
			p1 = new Point2D.Double(line.x1, line.y1);
		if (line.x2 >= x1 && line.x2 < x2 && line.y2 >= y1 && line.y2 < y2)
			p1 = new Point2D.Double(line.x2, line.y2);
		
		p2 = getIntersectionPoint(line, new Line2D.Double(x1,y1,x1,y2));
		if (p2 != null) {
			if (p1 != null)
				return new Line2D.Double(p1, p2);
			p1 = p2;
		}
		p2 = getIntersectionPoint(line, new Line2D.Double(x1,y2,x2,y2));
		if (p2 != null) {
			if (p1 != null)
				return new Line2D.Double(p1, p2);
			p1 = p2;
		}
		p2 = getIntersectionPoint(line, new Line2D.Double(x2,y2,x2,y1));
		if (p2 != null) {
			if (p1 != null)
				return new Line2D.Double(p1, p2);
			p1 = p2;
		}
		p2 = getIntersectionPoint(line, new Line2D.Double(x2,y1,x1,y1));
		if (p2 != null) {
			if (p1 != null)
				return new Line2D.Double(p1, p2);
			p1 = p2;
		}
		
		
		return null;
	}
	
	@Override
	public Polygon translateTriangle(double x1, double y1, double x2, double y2, double x3, double y3) {
		Point[] points = new Point[3];
		points[0] = translateOffscreenPoint(x1, y1);
		points[1] = translateOffscreenPoint(x2, y2);
		points[2] = translateOffscreenPoint(x3, y3);
		Rectangle screenView = translateRectangle(currentGridOffsetX, currentGridOffsetY, currentGridSize, currentGridSize);
		Polygon polygon = PolygonClipping.Clip(
				points[0].x, points[0].y,
				points[1].x, points[1].y,
				points[2].x, points[2].y,
				screenView.x, screenView.y, screenView.x + screenView.width, screenView.y + screenView.height);
		return polygon;
	}

}
