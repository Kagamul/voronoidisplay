package voronoi.grid.clipping;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

public class PolygonClipping {

	public static Polygon Clip(int x1, int y1, int x2, int y2, int x3, int y3, int screenX1, int screenY1, int screenX2, int screenY2) {
		Polygon polygon = new Polygon();
		List<Point> subjectP = new ArrayList<Point>();
		subjectP.add(new Point(x1, y1, 0));
		subjectP.add(new Point(x2, y2, 0));
		subjectP.add(new Point(x3, y3, 0));
		List<Edge> clippingP = new ArrayList<Edge>();
		clippingP.add(new Edge(new Point(screenX1, screenY1, 0), new Point(screenX1, screenY2, 0)));
		clippingP.add(new Edge(new Point(screenX1, screenY2, 0), new Point(screenX2, screenY2, 0)));
		clippingP.add(new Edge(new Point(screenX2, screenY2, 0), new Point(screenX2, screenY1, 0)));
		clippingP.add(new Edge(new Point(screenX2, screenY1, 0), new Point(screenX1, screenY1, 0)));
		List<Point> points = clippingAlg(subjectP, clippingP);
		for (Point p : points) {
			polygon.addPoint((int)Math.round(p.getX()), (int)Math.round(p.getY()));
		}
		return polygon;
	}
	
	@SuppressWarnings("unchecked")
	private static List<Point> clippingAlg(List<Point> subjectP ,List<Edge> clippingP){
	    List<Edge> edges = clippingP;
	    List<Point> in = subjectP;
	    ArrayList<Point> out = new ArrayList<Point>();

	    //begin looping through edges and points
	    int casenum = 0;
	    Point s = null;
	    for(int i=0;i<edges.size();i++){
	      Edge e = edges.get(i);
	      Point r = edges.get((i+2)%edges.size()).getP1();
	      if (in.size() > 0)
	    	  s = in.get(in.size()-1);
	      for(int j=0;j<in.size();j++){
	        Point p = in.get(j);

	        //first see if the point is inside the edge
	        if(Edge.isPointInsideEdge(e,r,p)){
	          //then if the specific pair of points is inside
	          if(Edge.isPointInsideEdge(e,r,s)){
	            casenum = 1;
	          //pair goes outside, so one point still inside
	          }else{
	            casenum = 4;
	          }

	        //no point inside
	        }else{
	          //does the specific pair go inside
	          if(Edge.isPointInsideEdge(e,r,s)){
	            casenum = 2;
	          //no points in pair are inside
	          }else{
	            casenum = 3;
	          }
	        }

	        switch(casenum){

	          //pair is inside, add point
	          case 1:
	            out.add(p);
	            break;

	          //pair goes inside, add intersection only
	          case 2:
	            Point inter0 = e.computeIntersection(s,p);
	            out.add(inter0);
	            break;

	          //pair outside, add nothing
	          case 3:
	            break;

	          //pair goes outside, add point and intersection
	          case 4:
	            Point inter1 = e.computeIntersection(s,p);
	            out.add(inter1);
	            out.add(p);
	            break;
	        }
	        s = p;
	      }
	      in = (List<Point>) out.clone();
	      out.clear();
	    }
	    return in;
	  }
	
}
