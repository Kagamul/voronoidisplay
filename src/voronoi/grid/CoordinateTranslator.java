package voronoi.grid;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

// translates coordinates from an euklidean space to on-screen coordinates
public interface CoordinateTranslator {

	public double translateDistance(int dist); // Translates on-screen distance to grid-distance
	
	public Point translatePoint(double x, double y); // Translates Point coordinates to screen coordinates, null if out of screen
	
	public Point translateOffscreenPoint(double x, double y); // Translates Point coordinates to screen coordinates
	
	public Point2D.Double translatePoint(int x, int y); // translates screen coordinates to grid coordinates, null if not in grid
	
	public Point[] translateLine(double x1, double y1, double x2, double y2); // Translates Line coordinates to screen coordinates, dimensions always 2
	
	public Rectangle translateRectangle(double x, double y, double width, double height); // Translates Rectangle coordinates to screen coordinates
	
	public Rectangle2D.Double getCurrentGridView();
	
	public void setGridView(Rectangle2D.Double gridView);
	
	public double getMaxGridSize();
	
	public void setMaxGridSize(double maxGridSize);
	
	public int getCurrentScreenSize();
	
	public void setScreenSize(int width, int height);
	
	public Polygon translateTriangle(double x1, double y1, double x2, double y2, double x3, double y3);
	
}
