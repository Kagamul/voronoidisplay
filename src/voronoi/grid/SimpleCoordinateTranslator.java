package voronoi.grid;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class SimpleCoordinateTranslator implements CoordinateTranslator{

	private final int screenSize;
	private double gridSize;
	private final int screenOffsetX, screenOffsetY;
	
	public SimpleCoordinateTranslator(int width, int height, double gridSize) {
		this.gridSize = gridSize;

		if (width > height) {
			screenSize = height-1;
			screenOffsetX = (width - height) / 2;
			screenOffsetY = 0;
		} else {
			screenSize = width-1;
			screenOffsetY = (height - width) / 2;
			screenOffsetX = 0;
		}
	}
	
	@Override
	public double translateDistance(int distance) {
		return (double)distance * gridSize / (double)screenSize ;
	}
	
	@Override
	public Point translatePoint(double x, double y) {
		if (x < 0 || y < 0 || x > gridSize || y > gridSize)
			return null;
		return translateOffscreenPoint(x, y);
	}
	
	@Override
	public Point translateOffscreenPoint(double x, double y) {
		final int px = (int)Math.round(screenSize * x / gridSize) + screenOffsetX;
		final int py = (int)Math.round(screenSize * y / gridSize) + screenOffsetY;
		return new Point(px,screenSize -py);
	}

	@Override
	public Point[] translateLine(double x1, double y1, double x2, double y2) {
		final Point[] point = new Point[2];
		point[0] = null;
		point[1] = null;
		int px, py;
		px = (int)Math.round((double)screenSize * x1 / gridSize) + screenOffsetX;
		py = (int)Math.round((double)screenSize * y1 / gridSize) + screenOffsetY;
		point[0] = new Point(px, screenSize - py);
		px = (int)Math.round((double)screenSize * x2 / gridSize) + screenOffsetX;
		py = (int)Math.round((double)screenSize * y2 / gridSize) + screenOffsetY;
		point[1] = new Point(px, screenSize - py);
		return point;
	}
	
	@Override
	public Rectangle translateRectangle(double x, double y, double width, double height) {
		int rectangleX, rectangleY, rectangleWidth, rectangleHeight;
		double x2 = x + width;
		double y2 = y + height;
		if (x2 < 0 || x > gridSize || y2 < 0 || y > gridSize)
			return null;
		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		if (x2 > gridSize)
			x2 = gridSize;
		if (y2 > gridSize)
			y2 = gridSize;
		rectangleX = (int)Math.round(screenSize * x / gridSize) + screenOffsetX;
		rectangleY = (int)Math.round(screenSize * y / gridSize) - screenOffsetY;
		rectangleWidth = (int)Math.round(screenSize * x2 / gridSize) + screenOffsetX - rectangleX;
		rectangleHeight = (int)Math.round(screenSize * y2 / gridSize) - screenOffsetY - rectangleY;
		return new Rectangle(rectangleX, rectangleY, rectangleWidth, rectangleHeight);
	}
	
	@Override
	public void setMaxGridSize(double gridSize) {
		this.gridSize = gridSize;
	}

	@Override
	public Rectangle2D.Double getCurrentGridView() {
		return new Rectangle2D.Double(0,0,gridSize,gridSize); // SimpleCoordinateTranslator has no scrolling capabilities, so always the same
	}

	@Override
	public double getMaxGridSize() {
		return gridSize;
	}

	@Override
	public int getCurrentScreenSize() {
		return screenSize; // SimpleCoordinateTranslator doesn't support window resizing, so always the same
	}

	@Override
	public void setScreenSize(int width, int height) {
		// not supported
	}
	
	@Override
	public Point2D.Double translatePoint(int x, int y) {
		x -= screenOffsetX;
		y += screenOffsetY;
		if (x < 0 || y < 0 || x >= screenSize || y >= screenSize) // Point outside of grid
			return null;
		final double gx = (double) x * gridSize / (double) screenSize;
		final double gy = (double) y * gridSize / (double) screenSize;
		return new Point2D.Double(gx, gridSize - gy);
	}

	@Override
	public void setGridView(Rectangle2D.Double gridView) {
		// not supported
	}
	
	@Override
	public Polygon translateTriangle(double x1, double y1, double x2, double y2, double x3, double y3) {
		Polygon polygon = new Polygon();
		Point point = translateOffscreenPoint(x1, y1);
		polygon.addPoint(point.x, point.y);
		point = translateOffscreenPoint(x2, y2);
		polygon.addPoint(point.x, point.y);
		point = translateOffscreenPoint(x3, y3);
		polygon.addPoint(point.x, point.y);
		return polygon;
	}

}
