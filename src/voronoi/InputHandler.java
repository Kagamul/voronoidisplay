package voronoi;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.Cursor;

public class InputHandler implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private int mouseX = 0, mouseY = 0;
	private int dragMouseX,dragMouseY;
	
	@Override
	public void keyPressed(KeyEvent event) {
		if (event.getExtendedKeyCode() ==  KeyEvent.VK_ESCAPE)
			VoronoiDisplay.getInstance().dispatchEvent(new WindowEvent(VoronoiDisplay.getInstance(), WindowEvent.WINDOW_CLOSING));
	}

	@Override
	public void keyReleased(KeyEvent event) {
	}

	@Override
	public void keyTyped(KeyEvent event) {
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent event) {
		final int notches = event.getWheelRotation();
		final double zoomLevel = 1.0 + 0.25*notches;
		VoronoiDisplay.getInstance().handleZoom(zoomLevel, mouseX, mouseY);
	}

	@Override
	public void mouseDragged(MouseEvent event) {
		mouseX = event.getX();
		mouseY = event.getY();
		int draggedX = dragMouseX - event.getX();
		int draggedY = event.getY() - dragMouseY;
		dragMouseX -= draggedX;
		dragMouseY += draggedY;
		if (draggedX != 0 || draggedY != 0) {
			VoronoiDisplay.getInstance().handleDrag(draggedX, draggedY);
		}
	}

	@Override
	public void mouseMoved(MouseEvent event) {
		mouseX = event.getX();
		mouseY = event.getY();
	}

	@Override
	public void mouseClicked(MouseEvent event) {
	}

	@Override
	public void mouseEntered(MouseEvent event) {
	}

	@Override
	public void mouseExited(MouseEvent event) {
	}

	@Override
	public void mousePressed(MouseEvent event) {
		if (event.getButton() == MouseEvent.BUTTON1) {
			dragMouseX = event.getX();
			dragMouseY = event.getY();
			VoronoiDisplay.getInstance().setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		}
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		if (event.getButton() == MouseEvent.BUTTON1) {
			int draggedX = dragMouseX - event.getX();
			int draggedY = event.getY() - dragMouseY;
			if (draggedX != 0 || draggedY != 0) {
				VoronoiDisplay.getInstance().handleDrag(draggedX, draggedY);
			}
			VoronoiDisplay.getInstance().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

}
