package voronoi.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import voronoi.grid.CoordinateTranslator;

public class DrawableLine2D implements DrawableElement {

	private static final Color lineColor = new Color(0,0,0);
	
	private double x1, y1, x2, y2;
	
	public DrawableLine2D(double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	@Override
	public void draw(Graphics2D graphics2D, CoordinateTranslator translator) {
		graphics2D.setColor(lineColor);
		Point[] points = translator.translateLine(x1,y1,x2,y2);
		if (points[0] == null)
			return;
		graphics2D.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
	}
	
	public double getX1() {
		return x1;
	}
	
	public double getX2() {
		return x2;
	}
	
	public double getY1() {
		return y1;
	}
	
	public double getY2() {
		return y2;
	}

}
