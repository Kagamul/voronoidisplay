package voronoi.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import voronoi.grid.CoordinateTranslator;

public class DrawableTriangle2D implements DrawableElement{
	
	private final double[] coords = new double[6];
	private final DrawablePoint2D point;
	
	public DrawableTriangle2D(DrawablePoint2D point, DrawableLine2D line) {
		coords[0] = point.getX();
		coords[1] = point.getY();
		coords[2] = line.getX1();
		coords[3] = line.getY1();
		coords[4] = line.getX2();
		coords[5] = line.getY2();
		this.point = point;
		
	}
	
	@Override
	public void draw(Graphics2D graphics2d, CoordinateTranslator translator) {
		Polygon polygon = translator.translateTriangle(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
		if (polygon == null)
			return;
		if (polygon.npoints < 3)
			return;
		Color c = point.getColor();
		graphics2d.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 50));
		graphics2d.fillPolygon(polygon);
	}

}
