package voronoi.graphics;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import voronoi.constants.Colors;
import voronoi.constants.Fonts;
import voronoi.grid.CoordinateTranslator;

public class DrawableText implements DrawableElement{

	final Point2D.Double position;
	final String text;
	
	public DrawableText(double positionX, double positionY, String text) {
		this.text = text;
		position = new Point2D.Double(positionX, positionY);
	}
	
	@Override
	public void draw(Graphics2D graphics2D, CoordinateTranslator translator) {
		Rectangle2D.Double gridView = translator.getCurrentGridView();
		if (!gridView.contains(position))
			return;
		
		Rectangle screenView = translator.translateRectangle(gridView.x, gridView.y, gridView.width, gridView.height);
		
		graphics2D.setFont(Fonts.smallTextFont);
		
		Point screenPosition = translator.translatePoint(position.x, position.y);
		
		Rectangle textBounds = Fonts.smallTextFont.createGlyphVector(graphics2D.getFontRenderContext(), text).getPixelBounds(null, 0, 0);
		
		int posX = screenPosition.x + 4;
		int posY = screenPosition.y - 4;
		
		if (posX + textBounds.width + 1 > screenView.x + screenView.width)
			posX = screenView.x + screenView.width - textBounds.width - 1;
		
		if (posY - textBounds.height - 1 < screenView.y)
			posY = screenView.y + textBounds.height + 1;
		
		graphics2D.setColor(Colors.textBackgroundColor);
		graphics2D.fillRect(posX, posY - textBounds.height, textBounds.width, textBounds.height);
		graphics2D.setColor(Colors.gridBoundaryColor);
		graphics2D.drawString(text, posX, posY);
		
	}

}
