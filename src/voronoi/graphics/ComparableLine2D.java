package voronoi.graphics;

public class ComparableLine2D {
	public final int p1, p2;
	
	public ComparableLine2D(int p1, int p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	@Override
	public int hashCode() {
		return p1 & p2;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ComparableLine2D))
			return false;
		ComparableLine2D line = (ComparableLine2D) o;
		if (line.p1 == p1)
			if (line.p2 == p2)
				return true;
			else 
				return false;
		else
			if (line.p1 == p2)
				if (line.p2 == p1)
					return true;
				else
					return false;
			else
				return false;
	}
	
}
