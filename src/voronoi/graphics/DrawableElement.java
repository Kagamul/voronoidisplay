package voronoi.graphics;

import java.awt.Graphics2D;

import voronoi.grid.CoordinateTranslator;

public interface DrawableElement {
	
	public void draw(Graphics2D graphics2D, CoordinateTranslator translator);
	
}
