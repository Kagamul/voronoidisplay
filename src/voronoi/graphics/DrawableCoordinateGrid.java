package voronoi.graphics;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import voronoi.constants.Colors;
import voronoi.constants.Fonts;
import voronoi.grid.CoordinateTranslator;

public class DrawableCoordinateGrid implements DrawableElement {
	
	@Override
	public void draw(Graphics2D graphics2D, CoordinateTranslator translator) {
		graphics2D.setColor(Colors.gridBackgroundColor);
		graphics2D.setFont(Fonts.genericTextFont);
		FontMetrics fontMetrics = graphics2D.getFontMetrics();
		Rectangle2D.Double gridView = translator.getCurrentGridView();
		Rectangle screenView = translator.translateRectangle(gridView.x, gridView.y, gridView.width, gridView.height);
		final int minx = screenView.x;
		final int miny = screenView.y;
		final int maxx = screenView.width + screenView.x;
		final int maxy = screenView.height + screenView.y;
		graphics2D.fillRect(screenView.x, screenView.y, screenView.width, screenView.height);
		double[] stepsX = getSteps(gridView.width);
		double stepsizeOffX = getStepOffset(stepsX, gridView.x);
		double[] stepsY = getSteps(gridView.height);
		double stepsizeOffY = getStepOffset(stepsY, gridView.y);
		graphics2D.setColor(Colors.secondaryGridColor);
		Point[] points = new Point[2];
		
		stepsX[0] = stepsX[0] * 0.1;
		stepsY[0] = stepsY[0] * 0.1;
		
		for (int a = 1; a <= 10*stepsX[1]; a++) {
			if (stepsizeOffX + stepsX[0]*a > gridView.x && stepsizeOffX + stepsX[0]*a <= gridView.x + gridView.width ) {
				points = translator.translateLine(stepsizeOffX + stepsX[0]*a, gridView.y, stepsizeOffX + stepsX[0]*a, gridView.y+gridView.height);
				graphics2D.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
			}
		}
		for (int a = 1; a <= 10*stepsY[1]; a++) {
			if (stepsizeOffY + stepsY[0]*a > gridView.y && stepsizeOffY + stepsY[0]*a <= gridView.y + gridView.height ) {
				points = translator.translateLine(gridView.x, stepsizeOffY + stepsY[0]*a, gridView.x + gridView.width, stepsizeOffY + stepsY[0]*a);
				graphics2D.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
			}
		}
		
		stepsX[0] = stepsX[0] * 10;
		stepsY[0] = stepsY[0] * 10;
		String text;
		Rectangle2D textBounds;
		
		graphics2D.setColor(Colors.primaryGridColor);
		for (int a = 1; a <= stepsX[1]; a++) {
			if (stepsizeOffX + stepsX[0]*a >= gridView.x && stepsizeOffX + stepsX[0]*a <= gridView.x + gridView.width ) {
				points = translator.translateLine(stepsizeOffX + stepsX[0]*a, gridView.y, stepsizeOffX + stepsX[0]*a, gridView.y+gridView.height);
				graphics2D.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
			}
		}
		for (int a = 1; a <= stepsY[1]; a++) {
			if (stepsizeOffY + stepsY[0]*a > gridView.y && stepsizeOffY + stepsY[0]*a <= gridView.y + gridView.height ) {
				points = translator.translateLine(gridView.x, stepsizeOffY + stepsY[0]*a, gridView.x + gridView.width, stepsizeOffY + stepsY[0]*a);
				graphics2D.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
			}
		}
		
		
		for (int a = 1; a <= stepsX[1]; a++) {
			if (stepsizeOffX + stepsX[0]*a >= gridView.x && stepsizeOffX + stepsX[0]*a <= gridView.x + gridView.width ) {
				points = translator.translateLine(stepsizeOffX + stepsX[0]*a, gridView.y, stepsizeOffX + stepsX[0]*a, gridView.y+gridView.height);
				text = String.valueOf(stepsizeOffX + stepsX[0]*a);
				textBounds = fontMetrics.getStringBounds(text, graphics2D);
				if (textBounds.getWidth() + points[0].x + 6 <= maxx) {
					graphics2D.setColor(Colors.textBackgroundColor);
					graphics2D.fillRect(points[0].x + 6, (points[0].y) - (int)textBounds.getHeight(), (int)textBounds.getWidth(), (int)textBounds.getHeight());
					graphics2D.setColor(Colors.gridBoundaryColor);
					graphics2D.drawString(text, points[0].x + 6, (points[0].y) - 3);
				}
			}
		}
		for (int a = 1; a <= stepsY[1]; a++) {
			if (stepsizeOffY + stepsY[0]*a > gridView.y && stepsizeOffY + stepsY[0]*a <= gridView.y + gridView.height ) {
				points = translator.translateLine(gridView.x, stepsizeOffY + stepsY[0]*a, gridView.x + gridView.width, stepsizeOffY + stepsY[0]*a);
				text = String.valueOf(stepsizeOffY + stepsY[0]*a);
				textBounds = fontMetrics.getStringBounds(text, graphics2D);
				if (textBounds.getHeight() + points[1].y + 15 <= maxy) {
					graphics2D.setColor(Colors.textBackgroundColor);
					graphics2D.fillRect(points[0].x + 6, (points[1].y) + 18 - (int)textBounds.getHeight(), (int)textBounds.getWidth(), (int)textBounds.getHeight());
					graphics2D.setColor(Colors.gridBoundaryColor);
					graphics2D.drawString(text, points[0].x + 6, points[1].y + 15);
				}
			}
		}
		graphics2D.setColor(Colors.gridBoundaryColor);
		graphics2D.drawRect(minx, miny, maxx - minx, maxy - miny);
	}
	
	private double[] getSteps(double dimension) {
		double[] steps = new double[2];
		if (dimension <= 0)
			return steps;
		
		double stepSize = 1;
		if (dimension > 1 && dimension <= 10) {
			if (dimension <= 3) {
				steps[0] = stepSize/2;
				steps[1] = 20;
			} else {
				if (dimension > 7) {
					steps[0] = stepSize * 2;
					steps[1] = 6;
				} else {
					steps[0] = stepSize;
					steps[1] = 10;
				}
			}
			
			return steps;
		}
		if (dimension < 1) {
			while (true) {
				stepSize = stepSize * 0.1;
				if (dimension > stepSize && dimension <= stepSize*10) {
					if (dimension <= stepSize*3) {
						steps[0] = stepSize/2;
						steps[1] = 20;
					} else {
						if (dimension > stepSize*7) {
							steps[0] = stepSize * 2;
							steps[1] = 6;
						} else {
							steps[0] = stepSize;
							steps[1] = 10;
						}
					}
					return steps;
				}
			}
		}
		while (true) {
			stepSize = stepSize * 10;
			if (dimension > stepSize && dimension <= stepSize*10) {
				if (dimension <= stepSize*3) {
					steps[0] = stepSize/2;
					steps[1] = 20;
				} else {
					if (dimension > stepSize*7) {
						steps[0] = stepSize * 2;
						steps[1] = 6;
					} else {
						steps[0] = stepSize;
						steps[1] = 10;
					}
				}
				return steps;
			}
		}
	}
	
	private double getStepOffset(double[] steps, double offset) {
		return ((int)(offset / steps[0])) * steps[0];
	}

}
