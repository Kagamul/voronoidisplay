package voronoi.graphics;

import java.awt.Color;

public class ColorGenerator {

	private int counter, num;
	
	public ColorGenerator(int num) {
		this.num = num;
		this.counter = 0;
	}
	
	public Color generateColor() {
		double stepsize = (2.0d * Math.PI) / (double) num;
		double angle = stepsize * counter;
		int r = (int)((Math.sin(angle) + 1) * 350) % 255;
		int g = (int)((Math.cos(angle) + 1) * 450) % 255;
		int b = (int)Math.abs(Math.sin(stepsize*num - angle) * 400) % 255;
		counter = (counter + 1) % num;
		return new Color(r,g,b);
	}
	
}
