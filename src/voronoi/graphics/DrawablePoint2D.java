package voronoi.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import voronoi.grid.CoordinateTranslator;

public class DrawablePoint2D implements DrawableElement {
	
	private static final int radius = 8;
	
	private double x, y;
	private Color color;
	private final boolean isSolid;
	
	public DrawablePoint2D(double x, double y) {
		this(x, y, true);
	}
	
	public DrawablePoint2D(double x, double y, boolean solid) {
		this.x = x;
		this.y = y;
		color = new Color(0,0,0);
		isSolid = solid;
	}
	
	public DrawablePoint2D(double x, double y, int r, int g, int b, boolean solid) {
		this.x = x;
		this.y = y;
		color = new Color(r,g,b);
		this.isSolid = solid;
	}
	
	@Override
	public void draw(Graphics2D graphics2D, CoordinateTranslator translator) {
		graphics2D.setColor(color);
		Point p = translator.translatePoint(x, y);
		if (p == null)
			return;
		if (isSolid)
			graphics2D.fillOval(p.x - radius/2, p.y - radius/2, radius, radius);
		else
			graphics2D.drawOval(p.x - radius/2, p.y - radius/2, radius, radius);
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

}
