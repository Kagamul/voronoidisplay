package voronoi.persistence;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import voronoi.VoronoiDisplay;
import voronoi.graphics.ColorGenerator;
import voronoi.graphics.ComparableLine2D;
import voronoi.graphics.DrawableLine2D;
import voronoi.graphics.DrawablePoint2D;
import voronoi.graphics.DrawableText;
import voronoi.graphics.DrawableTriangle2D;

public class VoronoiFileParser {

	private static int pointNumber = 1;
	
	public static void parse(String filePath) {
		int type = -1;
		int status = 0;
		String line;
		DrawablePoint2D dpoint2d;
		DrawableLine2D dline2d;
		ComparableLine2D line2d;
		double[] d = new double[4];
		int[] i = new int[2];
		VoronoiDisplay instance = VoronoiDisplay.getInstance();
		try {
			DataInputStream fileInputStream = new DataInputStream(new FileInputStream(filePath));
			BufferedReader fileReader = new BufferedReader(new InputStreamReader(fileInputStream));
			
			
			while ((line = fileReader.readLine()) != null) {
				switch (type) {
				case -1:
					d[0] = Double.parseDouble(line);
					System.out.println("gridsize: "+d[0]);
					instance.setMaxGridSize(d[0]);
					type = 0;
					break;
				case 0:
					type = Integer.parseInt(line);
					if (type == 0) {
						fileReader.close();
						System.out.println("Successfully parsed File!");
						int num = instance.getVoronoiPoints();
						
						ColorGenerator cGen = new ColorGenerator(num);
						
						for (int j = 1; j <= num; j++) {
							dpoint2d = instance.getVoronoiPoint(j);
							dpoint2d.setColor(cGen.generateColor());
						}
						return;
					}
					status = 0;
					break;
				case 1:
					d[status] = Double.parseDouble(line);
					status ++;
					if (status == 2) {
						System.out.println("adding point: "+d[0]+"|"+d[1]);
						dpoint2d = new DrawablePoint2D(d[0], d[1], true);
						instance.addVoronoiPoint(dpoint2d);
						instance.addDrawableComponent(dpoint2d);
						instance.addDrawableComponent(new DrawablePoint2D(d[0], d[1], 0, 0, 0, false));
						instance.addDrawableComponent(new DrawableText(d[0], d[1], "P" + pointNumber));
						pointNumber++;
						type = 0;
					}
					break;
				case 2:
					d[status] = Double.parseDouble(line);
					status ++;
					if (status == 2) {
						System.out.println("adding vertex: "+d[0]+"|"+d[1]);
						instance.addDrawableComponent(new DrawablePoint2D(d[0], d[1], 0, 0, 0, false));
						type = 0;
					}
					break;
				case 3:
					if (status < 2)
						i[status] = Integer.parseInt(line);
					else
						d[status - 2] = Double.parseDouble(line);
					status ++;
					if (status == 6) {
						line2d = new ComparableLine2D(i[0], i[1]);
						if (instance.addLine2D(line2d) || i[0] == 0 || i[1] == 0) {
							System.out.println("adding line: "+d[0]+"|"+d[1]+" - "+d[2]+"|"+d[3]);
							dline2d = new DrawableLine2D(d[0], d[1], d[2], d[3]);
							if (i[0] != 0) {
								dpoint2d = instance.getVoronoiPoint(i[0]);
								instance.addDrawableComponent(new DrawableTriangle2D(dpoint2d, dline2d));
							}
							if (i[1] != 0) {
								dpoint2d = instance.getVoronoiPoint(i[1]);
								instance.addDrawableComponent(new DrawableTriangle2D(dpoint2d, dline2d));
							}
							instance.addDrawableComponent(dline2d);
						}
						type = 0;
					}
					break;
				}
			}
			System.out.println("Error while reading input file: Unexpected end of file!");
			fileReader.close();
			
			
			
			return;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
